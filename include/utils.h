#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <string>
#include <iostream>
#include <map>

#include "BF.h"
#include "k_bloom_filter.h"

std::vector<std::string> parse_fastq_file(std::string input_fastq_file);

std::vector<std::string> create_kmers(std::vector<std::string> reads, int k);

void create_kmers_and_potential_edge_kmers(std::vector<std::string> reads, int k, std::vector<std::string> &kmers, std::vector<std::string> &potential_edge_kmers);

std::vector<std::string> create_queries(std::unordered_set<std::string> kmers, int k, int n, bool mutation);

void combination_repetition_util(std::vector<std::string> &combinations, std::string nucleotides[], std::string combination, int s, int n);

std::unordered_set<std::string> left_neighbours(std::vector<std::string> &combinations, std::string query_left);

std::unordered_set<std::string> right_neighbours(std::vector<std::string> &combinations, std::string query_right);

//bool check_contains_left(std::vector<std::string> &combinations, int s, std::string query, bf::basic_bloom_filter *bloom_filter);
bool check_contains_left(std::vector<std::string> &combinations, int s, std::string query, BF *bloom_filter);

//bool check_contains_right(std::vector<std::string> &combinations, int s, std::string query, bf::basic_bloom_filter *bloom_filter);
bool check_contains_right(std::vector<std::string> &combinations, int s, std::string query, BF *bloom_filter);

void evaluate(KBloomFilter*  kbf, int number_of_queries, bool mutation);
#endif // UTILS_H
