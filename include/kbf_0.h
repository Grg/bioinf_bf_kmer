#ifndef KBF0_H
#define KBF0_H

#include "k_bloom_filter.h"

#include "utils.h"

#include <map>

class KBF0: public KBloomFilter {

    public:
        KBF0 (int k) : KBloomFilter(k){};
        std::string name ();
        void populateFilter (std::vector<std::string> reads);
        bool contains (std::map<int, std::vector<std::string>> &combinations, std::string query);
};
#endif
