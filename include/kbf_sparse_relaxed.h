#ifndef KBFSPARSERELAXED_H
#define KBFSPARSERELAXED_H

#include "k_bloom_filter.h"
#include <unordered_set>

#include "bf/all.hpp"
#include "utils.h"
#include <map>
#include <set>

class KBFSparseRelaxed : public KBloomFilter {

private:
    std::unordered_set<std::string> potential_edge_kmers_;
    std::unordered_set<std::string> true_edge_kmers_;
    unsigned int s_;

public:
    KBFSparseRelaxed (int k, unsigned int s) : KBloomFilter(k), s_(s){};
    std::string name ();
    void populateFilter (std::vector<std::string> reads);
    bool contains (std::map<int, std::vector<std::string>> &combinations, std::string query);
    unsigned int s() { return s_; }
};
#endif
