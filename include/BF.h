#ifndef PROJECT_BF_H
#define PROJECT_BF_H

#include <iostream>
#include <vector>

typedef unsigned int (*HashFunction)(std::string);

class BF
{
        unsigned int numberOfCells;
        unsigned int numberOfFunctions;
        bool* cell;
        std::vector<HashFunction> hashFunctions;

    public:

        BF(unsigned int numbCells, std::vector<HashFunction> funcs) : numberOfCells(numbCells), hashFunctions(funcs)
        {
            cell = (bool*)calloc(numbCells, sizeof(bool));
        }

        void addElement(std::string str);

        bool searchElement(std::string str);

        ~BF()
        {
            free(cell);
            cell = NULL;
        }
};

// implementing a couple of hash functions for testing

unsigned int djb2(std::string str);

unsigned int sdbm(std::string str);


#endif //PROJECT_BF_H
