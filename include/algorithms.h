//
// Created by grg on 12/9/18.
//

#ifndef ALGORITHMS_H
#define ALGORITHMS_H

void kbf1();
void kbf2();
void strict_kbf();
void relaxed_kbf();

#endif //ALGORITHMS_H
