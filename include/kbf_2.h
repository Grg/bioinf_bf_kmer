#ifndef KBF2_H
#define KBF2_H

#include "k_bloom_filter.h"
#include <unordered_set>

#include "utils.h"
#include "bf/all.hpp"
#include <map>

class KBF2: public KBloomFilter {

    private:
        std::unordered_set<std::string> potential_edge_kmers_;
	    std::unordered_set<std::string> true_edge_kmers_;

    public:
        KBF2 (int k) : KBloomFilter(k){};
        std::string name ();
        void populateFilter (std::vector<std::string> reads);
        bool contains (std::map<int, std::vector<std::string>> &combinations, std::string query);
};
#endif
