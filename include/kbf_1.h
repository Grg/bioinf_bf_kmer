#ifndef KBF1_H
#define KBF1_H

#include "k_bloom_filter.h"

#include "bf/all.hpp"
#include "utils.h"

#include <map>

class KBF1: public KBloomFilter {

    public:
        KBF1 (int k) : KBloomFilter(k){};
        std::string name ();
        void populateFilter (std::vector<std::string> reads);
        bool contains (std::map<int, std::vector<std::string>> &combinations, std::string query);
};
#endif
