#ifndef KBLOOMFILTER_H
#define KBLOOMFILTER_H

#include <iostream>
#include "bf/all.hpp"
#include <unordered_set>

#include <iterator>
#include <BF.h>
#include <map>


class KBloomFilter
{

// basic Bloom FIlter construct
// or
// 1. @param Hasher
// 2. @param cells - the number of cells in a bit vector

protected:
//	bf::basic_bloom_filter* bloom_filter_;
    BF *bloom_filter_;
	int k_;
	int number_of_inserted_kmers_;
	std::unordered_set<std::string> kmers_;

public:
	KBloomFilter (int k);
	virtual void populateFilter (std::vector<std::string> reads) = 0;
	virtual bool contains (std::map<int, std::vector<std::string>> &combinations, std::string query) = 0;
    virtual std::string name () = 0;
	std::unordered_set<std::string> get_kmers() { return kmers_; }
	int k() { return k_; }
    int s() { return 1; }
};
#endif
