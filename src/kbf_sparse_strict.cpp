#include "kbf_sparse_strict.h"
#include "utils.h"

#include <vector>
#include <fstream>
#include <map>
#include <chrono>

//@author Ivan Matak
//checks whether query is in the Bloom filter or not
bool KBFSparseStrict::contains(std::map<int, std::vector<std::string>> &combinations, std::string query)
{
    //If the query is in the filter then check for left and right neighbours and if there is at least one on each side return true. 
    //If there is only neighbour on one side(left or right) check for edge k-mers and if the query is in the edge k-mers return true
    if(bloom_filter_ -> searchElement(query))
    {
        bool left = check_contains_left(combinations.at(s_ + 1), s_ + 1, query, bloom_filter_);
        bool right = check_contains_right(combinations.at(s_ + 1), s_ + 1, query, bloom_filter_);
        if(left && right)
        {
            return true;
        }
        if(left || right)
        {
            if(true_edge_kmers_.find(query) != true_edge_kmers_.end())
            {
                return true;
            }
        }
    }
    //if the query is not in the filter then check for neighbouring ones. If they exist
    //sum of their skip lengths should match s_-1 for true match.
    //Example: sequence S consists of k-mers: s0 s1 s2 s3 s4 s5 and skip length s_ is 6 then s0
    //and s5 would be in the filter. If we query for s2 we need to skip 1 on the left and 2 on the
    //right for true match 
    for(int i = 0; i < s_; i++)
    {
        bool left = check_contains_left(combinations.at(i + 1), i + 1, query, bloom_filter_);
        bool right = check_contains_right(combinations.at(s_ - i), s_ - i, query, bloom_filter_);
        if(left && right)
        {
            return true;
        }
        if(left || right)
        {
            if(true_edge_kmers_.find(query) != true_edge_kmers_.end())
            {
                return true;
            }
        }
    }
    return false;
}
//@author Ivan Matak
//Generates sparsified k-mers set and edge k-mers set and populates the Bloom filter 
void KBFSparseStrict::populateFilter(std::vector<std::string> reads)
{
    auto start = std::chrono::system_clock::now();
    int number_of_inserted_kmers = 0;
    for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++)
    {
        std::string read = *it;
        int read_length = read.length();
        if(read_length < k_)
        {
            continue;
        }

        std::vector<std::string> kmers_sequence;
        std::vector<int> starting_point(s_ + 1, 0);
        //gets all k-mers from read and calculates matches to
        //the current k-mer set
        for(int i = 0; i < read_length - k_ + 1; i++)
        {
            std::string kmer = read.substr(0, k_);
            kmers_sequence.push_back(kmer);
            number_of_inserted_kmers+=1;
            
            if(kmers_.find(kmers_sequence.at(i)) != kmers_.end()) //k-mer is already in current k-mer set
            { 
                starting_point[i % (s_ + 1)]++;
            }

            read = read.substr(1, read_length - i);
        }

        //determine starting position
        int start = 0;
        int max_value = starting_point[0];
        for(int i = 0; i < starting_point.size(); i++)
        {
            if(starting_point[i] > max_value)
            {   
                start = i;
                max_value = starting_point[i];
            }
        }

        //get sparse k-mer set starting from the start index and taking every s-th k-mer
        int last_added = 0;
        for(int i = 0; i < kmers_sequence.size(); i++)
        {
            if(i <= start) // maybe there are some edges before start
            {
                potential_edge_kmers_.insert(kmers_sequence.at(i));
            }
            
            if(!((i - start) % (s_ + 1))) 
            {
                kmers_.insert(kmers_sequence.at(i));
                //bloom_filter_ -> addElement(kmers_sequence.at(i));
                last_added = i;
            }
        }
        //add everything after the last added k-mer to potential edge k-mers 
        for(int i = last_added; i < kmers_sequence.size(); i++)
        {
            potential_edge_kmers_.insert(kmers_sequence.at(i));
        }
    }
    
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"Generating kmers: " << elapsed_seconds.count() << "s" << std::endl;

    std::cout << "Kmers size: " << kmers_.size() << '\n';
    std::vector<HashFunction> hashFunctions;
    hashFunctions.push_back(djb2);
    hashFunctions.push_back(sdbm);

    start = std::chrono::system_clock::now();
    bloom_filter_ = new BF((unsigned int) (10 *  kmers_.size()), hashFunctions); // 10 * number of kmers
    for (auto i = kmers_.begin(); i != kmers_.end(); ++i)  
    {
	    bloom_filter_ -> addElement(*i);
    }

    std::map<int, std::vector<std::string>> combinations;
    std::string nucleotides[4] = {"A", "C", "G", "T"};
          
    for(int i = 1; i <= (s_ + 1); i++) 
    {
        std::vector<std::string> combs;
        combination_repetition_util(combs, nucleotides, "", i, 4);
        combinations.insert({i, combs});
    }
    
    //perform generation of true edges from potential edges
    for (auto pot_edge_kmer : potential_edge_kmers_)
    {
	    if (contains(combinations, pot_edge_kmer) == false)
	    {
		    true_edge_kmers_.insert(pot_edge_kmer);
	    }
    }    
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout<<"Populating filter: " << elapsed_seconds.count() << "s" << std::endl;
    
    std::cout<<"Number of inserted kmers: " << number_of_inserted_kmers << std::endl;
    std::cout<<"Number of kmers: "<< kmers_.size() << std::endl;
    std::cout<<"Number of potential: "<<potential_edge_kmers_.size() << std::endl;
    std::cout<<"Number of true: "<<true_edge_kmers_.size() << std::endl;   
}

//void KBFSparseStrict::populateFilterFromSequence(std::vector<std::string> reads)
//{
//    for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++)
//    {
//        std::string read = *it;
//        std::string read_copy = *it;
//        int read_length = read.length();
//        if(read_length < k_)
//        {
//            continue;
//        }
//        
//        int last_added = 0;
//        for(int i = 0; i < read_length - k_ + 1; i++)
//        {
//            std::string kmer = read.substr(0, k_);
//            bloom_filter_ -> addElement(kmer);
//            if(!(i % (s_ + 1)))
//            {
//                kmers_.insert(kmer);
//                last_added = i;
//            }
//            if (i == 0 || i == read_length - k_)
//            {
//                potential_edge_kmers_.insert(kmer);
//            }
//            read = read.substr(1, read_length - i);
//        }
//        
//        read_copy = read_copy.substr(last_added, read_length - last_added);
//        for(int i = last_added; i < read_length - k_ + 1; i++)
//        {
//            std::string kmer = read.substr(0, k_);
//            potential_edge_kmers_.insert(kmer);
//            read_copy = read_copy.substr(1, read_length - i);
//        }
//    }
//
//    std::map<int, std::vector<std::string>> combinations;
//    std::string nucleotides[4] = {"A", "C", "G", "T"};
//          
//    for(int i = 1; i <= s_; i++) 
//    {
//        std::vector<std::string> combs;
//        combination_repetition_util(combs, nucleotides, "", i, 4);
//        combinations.insert({i, combs});
//    }
//    
//    for (auto pot_edge_kmer : potential_edge_kmers_)
//    {
//	    if (contains(combinations, pot_edge_kmer) == false)
//	    {
//		    true_edge_kmers_.insert(pot_edge_kmer);
//	    }
//    }
//}

std::string KBFSparseStrict::name(){ return "kbf_sparse_strict"; }
