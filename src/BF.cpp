#include "BF.h"
typedef unsigned int (*HashFunction)(std::string);

//Simple bloom filter function for adding one element (string) into it
void BF::addElement(std::string str)
{
    for (std::vector<HashFunction>::iterator iter = hashFunctions.begin(); iter != hashFunctions.end(); iter++) {
        cell[(*iter)(str) % numberOfCells] = true;
    }
}
//Simple bloom filter function for looking does filter contain element (string)
bool BF::searchElement(std::string str)
{
    bool strInSet = true;

    for (std::vector<HashFunction>::iterator iter = hashFunctions.begin(); iter != hashFunctions.end(); iter++)
    {
        if (cell[(*iter)(str) % numberOfCells] == false)
        {
            strInSet = false;
            break;
        }
    }

    return strInSet;
}


// implementing a couple of hash functions for testing


// Hash function 1
unsigned int djb2(std::string str)
{
    unsigned int hash = 5381;

    for (std::string::iterator iter = str.begin(); iter != str.end(); iter++)
    {
        hash = ((hash << 5) + hash) + *iter;
    }

    return hash;
}

// Hash function 2
unsigned int sdbm(std::string str)
{
    unsigned int hash = 0;

    for (std::string::iterator iter = str.begin(); iter != str.end(); iter++)
    {
        hash = ((hash << 6) + (hash << 16) - hash) + *iter;
    }

    return hash;
}


