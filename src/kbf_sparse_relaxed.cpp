#include "kbf_sparse_relaxed.h"
#include "utils.h"

#include <vector>
#include <fstream>
#include <algorithm>
#include <chrono>

/** Checks if a query is inside the bloom filter.
*
* Uses the kbf sparse relaxed variant.
*
*
* @param query the nucleotide to look for
* @param prefixes and suffixes to add to the k-mer when creating the neighbouring k-mer
*/
bool KBFSparseRelaxed::contains(std::map<int, std::vector<std::string>> &combinations, std::string query)
{
    if(bloom_filter_ -> searchElement(query))
    {
        bool left = false;
        bool right = false;
        for(unsigned i = 0; i < s_ + 1; i++)
        {
            left |= check_contains_left(combinations.at(i + 1), i + 1, query, bloom_filter_);
            right |= check_contains_right(combinations.at(i + 1), i + 1, query, bloom_filter_);

            if (left && right) return true;
        }

        if(left || right)
        {
            if(true_edge_kmers_.find(query) != true_edge_kmers_.end()) return true;
        }
        return false;
    }
    else
    {
        bool left = false;
        bool right = false;
        for (int i = 0; i < s_; i++)
        {
            left |= check_contains_left(combinations.at(i + 1), i + 1, query, bloom_filter_);
            right |= check_contains_right(combinations.at(s_ - i), s_ - i, query, bloom_filter_);
            if (left && right) return true;
        }

        if (left || right)
        {
            if (true_edge_kmers_.find(query) != true_edge_kmers_.end()) return true;
        }
        return false;
    }
}


/** Filles the bloom filter with needed k-mers.
*
*  Creates the needed k-mers from reads and inserts them in the bloom filter according to the hitting set strategy.
*
*
* @param reads reads to insert
*/
void KBFSparseRelaxed::populateFilter(std::vector<std::string> reads)
{
    
    auto start = std::chrono::system_clock::now();
    std::map <std::string, std::set<std::string>> left;
    std::map <std::string, std::set<std::string>> right;
    std::map <std::string, std::set<std::set<std::string>>> kmer_handles;
    std::vector <std::pair<std::string, std::set<std::set<std::string>>>> kmer_handles_vector;
    std::vector <std::string> read_kmers;

    std::unordered_set <std::string> all_kmers;
    std::unordered_set <std::string> not_potential_edge_kmers;

    for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++)
    {
        std::string read = *it;
        int read_length = read.length();
        if (read_length < k_) continue;

        read_kmers.clear();

        // get potentital edges
        for (int i = 0; i < read_length - k_ + 1; i++)
        {

            std::string kmer = read.substr(i, k_);

            if (i == 0 || i == read_length - k_)
            {
                potential_edge_kmers_.insert(kmer);
            }
            else
            {
                not_potential_edge_kmers.insert(kmer);
            }

            read_kmers.push_back(kmer);
            all_kmers.insert(kmer);
        }

        // create L and R sets
        for (int i = 0; i < read_length - k_ + 1; i++)
        {

            if (i != 0)
            {
                left[read_kmers[i]].insert(read_kmers[i - 1]);
            }

            if (i != read_length - k_) {
                right[read_kmers[i]].insert(read_kmers[i + 1]);
            }

            left[read_kmers[i]].insert(read_kmers[i]);
            right[read_kmers[i]].insert(read_kmers[i]);

        }
    }

    // solve the hitting set problem
    for(auto& km_set: left)
    {
        for(auto km: km_set.second)
        {
            (kmer_handles[km]).insert(km_set.second);
        }
    }
    for(auto& km_set: right)
    {
        for(auto km: km_set.second)
        {
            (kmer_handles[km]).insert(km_set.second);
        }
    }

    for (auto iterator = kmer_handles.begin(); iterator != kmer_handles.end(); ++iterator)
    {
            kmer_handles_vector.push_back(*iterator);
    }

    std::cout << "size:" << kmer_handles.size() << std::endl;
    std::cout << "size:" << kmer_handles_vector.size() << std::endl;
    std::cout << "sort_begin" << std::endl;

    // sort the kmers greedily
    // this differes from the paper, we use list sorted one and the authors use heap
    // we use list because it works faster for us and does not suffer a loss in fpr
    std::sort(kmer_handles_vector.begin(), kmer_handles_vector.end(),
        [] (
                std::pair<std::string, std::set<std::set<std::string>>> p1,
                std::pair<std::string, std::set<std::set<std::string>>> p2
        ){
            return p1.second.size() > p2.second.size();
        }
    );
    std::cout << "sort_done" << std::endl;
    std::string current_kmer;
    int i = 0;
    int deleted = 0;

    while( kmer_handles.size() > 0  and i < kmer_handles_vector.size() )
    {
        current_kmer = kmer_handles_vector[i].first;

        if (kmer_handles[current_kmer].size() == 0)
        {
            deleted += kmer_handles.erase(current_kmer);
            i += 1;
            continue;
        }

        auto p = kmer_handles_vector[i];

        current_kmer = p.first;


        kmers_.insert(current_kmer);

        for (auto neighbours : kmer_handles[current_kmer])
        {
            for(auto km: neighbours)
            {
                (kmer_handles[km]).erase(neighbours);
                if (kmer_handles[km].size() == 0)
                {
                    deleted += kmer_handles.erase(km);
                }
            }
        }
        deleted += kmer_handles.erase(current_kmer);
        i+=1;
    }
    
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"Generating kmers: " << elapsed_seconds.count() << "s" << std::endl;

    std::cout << "Kmers size: " << kmers_.size() << '\n';
    std::vector<HashFunction> hashFunctions;
    hashFunctions.push_back(djb2);
    hashFunctions.push_back(sdbm);
    
    // create the bloom filter and add k-mers to it
    start = std::chrono::system_clock::now();
    bloom_filter_ = new BF((unsigned int) (10 *  kmers_.size()), hashFunctions); // 10 * number of kmers
    for (auto i = kmers_.begin(); i != kmers_.end(); ++i)  
    {
	bloom_filter_ -> addElement(*i);
    }

    std::map<int, std::vector<std::string>> combinations;
    std::string nucleotides[4] = {"A", "C", "G", "T"};

    for(int i = 1; i <= (s_ + 1); i++)
    {
        std::vector<std::string> combs;
        combination_repetition_util(combs, nucleotides, "", i, 4);
        combinations.insert({i, combs});
    }

    // filter potentital edge k-mers to get true edge k-mers
    for (auto pot_edge_kmer : potential_edge_kmers_)
    {
        if (contains(combinations, pot_edge_kmer) == false)

        {
            true_edge_kmers_.insert(pot_edge_kmer);
        }
    }
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout<<"Populating filter: " << elapsed_seconds.count() << "s" << std::endl;

    std::cout<<"All kmers: "<< all_kmers.size() << std::endl;
    std::cout<<"Number of kmers: "<< kmers_.size() << std::endl;

    std::cout << "true_e:" << true_edge_kmers_.size() << std::endl;
    std::cout << "potential_e:" << potential_edge_kmers_.size() << std::endl;

}


std::string KBFSparseRelaxed::name(){ return "kbf_sparse_relaxed"; }
