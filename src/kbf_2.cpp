#include "kbf_2.h"

#include <vector>
#include <map>
#include <chrono>
//@author Ivan Matak
//checks whether query is in the Bloom filter or not. If it is not returns false right away. If it is then 
//it additionally checks if there is at least one neighbour on left side and one neighbour on the right side
//in Bloom filter and if it is then it returns true. If there is at least one neighbour in the filter only on 
//the one side(left or right) it checks whether the query is the true edge and if it is returns true, false otherwise.
bool KBF2::contains (std::map<int, std::vector<std::string>> &combinations, std::string query)
{
//  if(bloom_filter_ -> lookup(query))
    if(bloom_filter_ -> searchElement(query))
    {
        bool left = check_contains_left(combinations.at(1), 1, query, bloom_filter_);
        bool right = check_contains_right(combinations.at(1), 1, query, bloom_filter_);
        if(left && right)
        {
            return true;
        }
        else if(left || right)
        {
            if(true_edge_kmers_.find(query) != true_edge_kmers_.end())
            {
                return true;
            }
        }
    }
    return false;
}

////@author Franko
//Generates kmers from reads
//populate filter with all possible kmers (edge kmers too, we need them also in bloom filter)
// Also, we save all potential_edge_kmers
// It also looks for true edges from potential edge kmers
void KBF2::populateFilter(std::vector<std::string> reads)
{
    auto start = std::chrono::system_clock::now();
    // Iterate all reads
    for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++)
    {
        std::string read = *it;
        int read_length = read.length();
        // From every read we can get   L - K + 1  kmers (L = KENGTH, K = KMER SIZE)
        for (int i = 0; i < read_length - k_ + 1; i++)
        {
            std::string kmer = read.substr (0, k_);
//          bloom_filter_ -> add(kmer);
            //bloom_filter_ -> addElement(kmer);
            kmers_.insert(kmer);
            number_of_inserted_kmers_ += 1;


            // Additional query if this kmer is on the edge of the sequence (first or last)
            // That makes him potential edge kmer because sometimes because of shotgun sequencing that kmer can finish within other sequence
            // And we need to check that out later
            if (i == 0 || i == read_length - k_)
            {
                potential_edge_kmers_.insert (kmer);
            }
            read = read.substr (1, read_length - i);
        }

    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"Generating kmers: " << elapsed_seconds.count() << "s" << std::endl;

    std::cout << "Kmers size: " << kmers_.size() << '\n';
    std::vector<HashFunction> hashFunctions;
    hashFunctions.push_back(djb2);
    hashFunctions.push_back(sdbm);

    start = std::chrono::system_clock::now();
    bloom_filter_ = new BF((unsigned int) (10 *  kmers_.size()), hashFunctions); // 10 * number of kmers
    for (auto i = kmers_.begin(); i != kmers_.end(); ++i)  
    {
	    bloom_filter_ -> addElement(*i);
    }

    std::map<int, std::vector<std::string>> combinations;
    std::vector<std::string> combs = {"A", "C", "G", "T"};
    combinations.insert({1, combs});

    for (auto pot_edge_kmer : potential_edge_kmers_)
    {
	    if (contains(combinations, pot_edge_kmer) == false)
	    {
		    true_edge_kmers_.insert(pot_edge_kmer);
	    }
    }
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout<<"Populating filter: " << elapsed_seconds.count() << "s" << std::endl;

    std::cout << "Number of inserted kmers: " << number_of_inserted_kmers_ << '\n';
    std::cout << "Potential edge kmers: " << potential_edge_kmers_.size() << '\n';
    std::cout << "True edge kmers: " << true_edge_kmers_.size() << '\n';
}

std::string KBF2::name(){ return "kbf2"; }
