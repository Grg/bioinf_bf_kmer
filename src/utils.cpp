#include "utils.h"

#include <fstream>
#include <map>
#include <chrono>


//Function for parsing fastq file and taking sequences from sequence structures
//@author Franko
std::vector<std::string> parse_fastq_file(std::string input_fastq_file)
{
	std::vector<std::string> reads;
	std::ifstream file (input_fastq_file.c_str());
	std::string line;
	int counter = 0;
	// SEQUENCE STRUCTURE
	// 1. sequence name
	// 2. sequence
	// 3. + comment
	// 4. sequence quality
	if (file.is_open())
	{

		while (getline(file, line)) 
		{
			counter+= 1;
			// We only take sequence from sequence structure which is every second row (of 4 rows)
			if (counter % 4 == 2) 
			{
				reads.push_back (line);
			}
		}
		file.close();
	} else 
	{
		std::cout  << "Unable to open file";
	}
	return reads;
}

//Function for creating and returning kmers from raw reads
// Number of kmers from 1 read we can get is L - K + 1
//@author Franko
std::vector<std::string> create_kmers(std::vector<std::string> reads, int k)
{
	std::vector<std::string> kmers;
	// Iterate all reads
	for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++) 
	{
		std::string read = *it;
		int read_length = read.length();
		// From every read we can get   L - K + 1  kmers (L = KENGTH, K = KMER SIZE)
		for (int i = 0; i < read_length - k + 1; i++) 
		{
			std::string kmer = read.substr (0, k);
			kmers.push_back (kmer);
			read = read.substr (1, read_length - i);
		}
				
	}
	return kmers;
}

// We use this function for two-sided KBF (getting kmers and potential edge kmers)
//Same as create kmers, but here we are getting potential edge kmers
//@author Franko
void create_kmers_and_potential_edge_kmers(
		std::vector<std::string> reads,
		int k,
		std::vector<std::string> &kmers,
		std::vector<std::string> &potential_edge_kmers
){
	// Iterate all reads
	for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++) 
	{
		std::string read = *it;
		int read_length = read.length();
		// From every read we can get   L - K + 1  kmers (L = KENGTH, K = KMER SIZE)
		for (int i = 0; i < read_length - k + 1; i++) 
		{
			std::string kmer = read.substr(0, k);
			kmers.push_back(kmer);
			// Additional query if this kmer is on the edge of the sequence (first or last)
			// That makes him potential edge kmer because sometimes because of shotgun sequencing that kmer can finish within other sequence
			// And we need to check that out later
			if (i == 0 || i == read_length - k) 
			{
				potential_edge_kmers.push_back(kmer);
			}
			read = read.substr(1, read_length - i);
		}
				
	}
	
}

//Get queries from original kmers with /without mutation
//For mutation we are using 3 different uniform distributions
// 1. kmer distribution - which kmer from kmer set are we picking for query
// 2. k distribution - which position in kmer we want to mutate
// 3. nucleotide distribution - which nucleotide we want to use on that position (A, C, G, T)
//@author Franko
std::vector<std::string> create_queries(std::unordered_set<std::string> kmers_set, int k, int n, bool mutation)
{
	std::vector<std::string> queries;
	std::uniform_int_distribution<int> kmer_distribution(0, kmers_set.size() - 1);
	std::uniform_int_distribution<int> k_distribution(0, k-1);
	std::uniform_int_distribution<int> nucleotide_distribution(0, 3);
	std::default_random_engine generator(time(0));
	std::string nucleotides = "ACGT";

	std::vector<std::string> kmers;
	std::cout << "Creating queries" << '\n';
	for (auto kmer : kmers_set)
	{
		kmers.push_back (kmer);
	}
	for (int i = 0; i < n; i++)
	{
		int kmer_index = kmer_distribution(generator);
		std::string random_kmer = kmers[kmer_index];
		if (!mutation)
		{
			queries.push_back(random_kmer);
		}
		else
		{
			
			int position_in_kmer = k_distribution(generator);
			int nucleotide = nucleotide_distribution(generator);
			random_kmer[position_in_kmer] = nucleotides[nucleotide];
			while (random_kmer.compare (kmers[kmer_index]) == 0)
			{
				nucleotide = nucleotide_distribution(generator);
				random_kmer[position_in_kmer] = nucleotides[nucleotide];
			}
			queries.push_back(random_kmer);
		}	
	}
    
	std::cout << "Queries created." << '\n';
	return queries;	
}

//Method to print all possible  
//nuceotide strings of length s 
//@author Ivan
void combination_repetition_util(std::vector<std::string> &combinations, std::string nucleotides[], std::string combination, int s, int n) 
{
    
    // Base case: s is 0, 
    // add combination 
    if(s==0) 
    {
        combinations.push_back(combination);
        return;
    }
    
    // One by one add all nucleotides  
    // from array and recursively  
    // call for s equals to s-1 
    for (int i = 0; i < n; ++i) 
    { 
  
        // Next nucleotide added 
        std::string new_combination = combination + nucleotides[i];  
          
        // s is decreased, because  
        // we have added a new nucleotide 
        combination_repetition_util(combinations, nucleotides, new_combination, s - 1, n);  
    } 
}

//Calculates and returns neighbours on the left of the given query
//@author Ivan
std::unordered_set<std::string> left_neighbours(std::vector<std::string> &combinations, std::string query_left)
{
    std::unordered_set<std::string> left_neighbours_set;
    for(int i = 0; i < combinations.size(); i++)
    {
        left_neighbours_set.insert(combinations.at(i) + query_left);
    }

    return left_neighbours_set;
}

//Calculates and returns neighbours on the right of the given query
//@author Ivan
std::unordered_set<std::string> right_neighbours(std::vector<std::string> &combinations, std::string query_right)
{
    std::unordered_set<std::string> right_neighbours_set;
    for(int i = 0; i < combinations.size(); i++)
    {
       right_neighbours_set.insert(query_right + combinations.at(i));
    }

    return right_neighbours_set;
}

//Checks if any of the left neighbours is in Bloom filter
//bool check_contains_left(std::vector<std::string> &combinations, int s, std::string query, bf::basic_bloom_filter *bloom_filter)
//@author Ivan
bool check_contains_left(std::vector<std::string> &combinations, int s, std::string query, BF *bloom_filter)
{
    std::unordered_set<std::string> left = left_neighbours(combinations, query.substr(0, query.length() - s));
    std::unordered_set<std::string> :: iterator itrL;
    for (itrL = left.begin(); itrL != left.end(); itrL++)
    { 
//        if(bloom_filter -> lookup(*itrL))
        if(bloom_filter -> searchElement(*itrL))
        {
            return true;
        }
    }
    return false;
}

//Checks if any of the right neighbours is in Bloom filter
//bool check_contains_right(std::vector<std::string> &combinations, int s, std::string query, bf::basic_bloom_filter *bloom_filter)
//@author Ivan
bool check_contains_right(std::vector<std::string> &combinations, int s, std::string query, BF *bloom_filter)
{
    std::unordered_set<std::string> right = right_neighbours(combinations, query.substr(s));
    std::unordered_set<std::string> :: iterator itrR;
    for (itrR = right.begin(); itrR != right.end(); itrR++)
    { 
//        if(bloom_filter -> lookup(*itrR))
        if(bloom_filter -> searchElement(*itrR))
        {
            return true;
        }
    }
    return false;
}     

//Function for evaluating k bloom filter on certain number of queries (calculating false positive rate)
//@author Grgur
void evaluate(KBloomFilter* kbf, int number_of_queries, bool mutation) {
    std::unordered_set <std::string> kmers = kbf->get_kmers();
    std::cout << "Kmers size: " << kmers.size() << '\n'; // It is smaller size because there are duplicates
    std::vector <std::string> queries = create_queries(kmers, kbf->k(), number_of_queries, mutation);
    
    std::string name = kbf -> name();
    std::map<int, std::vector<std::string>> combinations;
    std::vector <std::string> combs;    

    if((name.compare("kbf1")==0) || (name.compare("kbf2")==0) || (name.compare("kbf0") == 0))
    {
        combs = {"A", "C", "G", "T"};
        combinations.insert({1, combs});
    }
    else
    {
	
        int s = kbf -> s();
        std::string nucleotides[4] = {"A", "C", "G", "T"};
        for(int i = 1; i <= (s + 1); i++)
        {
            combination_repetition_util(combs, nucleotides, "", i, 4);
            combinations.insert({i, combs});
        }
    }    

    std::ofstream results_file;
    results_file.open("../results/" + kbf->name() + "_results");

    int fp = 0;
    int total = 0;
    bool true_state, query_in_bf;

    auto start = std::chrono::system_clock::now();
    for (auto query_kmer: queries)
    {

        query_in_bf = kbf->contains(combinations, query_kmer);
        true_state = kmers.find(query_kmer) != kmers.end();

        if (query_in_bf and !true_state) { fp++; }
        if (!true_state) { total++; }

        results_file << query_kmer << '\t' << query_in_bf << '\t' << true_state << '\n';
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"Querying time: " << elapsed_seconds.count() << "s" << std::endl;
    

    std::cout << "fp/total = " << fp << "/" << total << std::endl;
    std::cout << "fpr: " << (float) fp / total << std::endl;
    results_file << "fpr: " << (float) fp / total << std::endl;
    results_file.close();
}
