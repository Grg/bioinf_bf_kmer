#include <iostream>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <chrono>

#include "k_bloom_filter.h" //to delete
#include "kbf_0.h"
#include "kbf_1.h"         //to delete
#include "kbf_2.h"         //to delete
#include "kbf_sparse_strict.h"
#include "kbf_sparse_relaxed.h"
#include "utils.h"
#include "algorithms.h"
#include "bf/all.hpp"
#include <map>

//Main program

//@author Franko
//@author Grgur
int main(int argc, char ** argv) {
    //   parse arguments
    //   1. Input fastq file,
    //   2. K - k-mer size
    //   3. num_queries
    //   4. true_positives

    if (argc < 3) {
        std::cerr << "Required rguments were not passed." << '\n';
        std::cerr << "Required arguments order is: " << '\n';
	    std::cerr << "1. number of algorithm - 0 = KBF0, 1 = KBF1, 2 = KBF2, 3 = KBFSparseStrict, ..." << '\n';
        std::cerr << "2. k - length of k-mer" << '\n';
        std::cerr << "3. N - number of queries " << '\n';
        std::cerr << "4. Mutation? - enter '-fp' if you want false positives << '\n'";
        exit(1);
    }

    auto start = std::chrono::system_clock::now();
    std::vector <std::string> reads = parse_fastq_file("../resources/ERR_2000000");
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"File parsing: " << elapsed_seconds.count() << "s" << std::endl;
    
    int algorithm = atoi(argv[1]);


    int k = atoi(argv[2]);
    int number_of_queries = atoi(argv[3]);

    bool mutation = false;
    if (argc == 5)
    {
        std::string flag = argv[4];
        mutation = flag == "-fp";

        if (!mutation ){
            std::cerr << "Wrong arguments" << '\n';
        }
    }
    std::cout << "algorithm: " << algorithm << '\n';
    std::cout << "k-mer lengths: " << k << '\n';
    std::cout << "number of queries: " << number_of_queries << '\n';
    std::cout << "Mutation: " << mutation << '\n';

    KBloomFilter * kbf;
    switch (algorithm) {
        case 0:
            {
                kbf = new KBF0(k);
                break;
            }
        case 1:
            {
                kbf = new KBF1(k);
                break;
            }
        case 2:
            {
                kbf = new KBF2(k);
                break;
            }
        case 3:
            {
                kbf = new KBFSparseStrict(k, 1);
                break;
            }
        case 4:
            {
                kbf = new KBFSparseRelaxed(k, 1);
                break;
            }
    }
    
    kbf -> populateFilter(reads);

    evaluate(kbf, number_of_queries, mutation);
    delete kbf;
    return 0;
}
