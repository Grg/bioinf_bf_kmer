#include "kbf_1.h"

#include <vector>
#include <fstream>
#include <map>
#include <chrono>

//@author Ivan Matak
//checks whether query is in the Bloom filter or not. If it is not returns false right away. If it is then 
//it additionally checks if there is at least one neighbour on left side or on the right side and if there 
//is at least one neighbour in filter contains returns true, false otherwise   
////@author Ivan
bool KBF1::contains (std::map<int, std::vector<std::string>> &combinations, std::string query)
{
//  if(bloom_filter_ -> lookup(query))
    if(bloom_filter_ -> searchElement(query))
    {
        if(check_contains_left(combinations.at(1), 1, query, bloom_filter_) || check_contains_right(combinations.at(1), 1 , query, bloom_filter_))
	    {
	    	return true;
	    }
	    return false;
    }
    return false;
}

////@author Franko
//Populate filter with kmers created from reads
//Calculate time for generating kmers and populating bloom filter

void KBF1::populateFilter(std::vector<std::string> reads)
{
    auto start = std::chrono::system_clock::now();
    // Iterate all reads
    for (std::vector<std::string>::iterator it = reads.begin(); it != reads.end(); it++)
    {
        std::string read = *it;
        int read_length = read.length();
        // From every read we can get   L - K + 1  kmers (L = KENGTH, K = KMER SIZE)
        for (int i = 0; i < read_length - k_ + 1; i++)
        {
            std::string kmer = read.substr (0, k_);
//          bloom_filter_ -> add(kmer);
            //bloom_filter_ -> addElement(kmer);
            kmers_.insert(kmer);
            number_of_inserted_kmers_ += 1;
            read = read.substr (1, read_length - i);
        }
//        std::cout << "storage:" <<  bloom_filter_ -> storage() << std::endl;

    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"Generating kmers: " << elapsed_seconds.count() << "s" << std::endl;
    std::cout << "Number of inserted kmers: " << number_of_inserted_kmers_ << '\n';
    std::cout << "Number of inserted kmers: " << number_of_inserted_kmers_ << '\n';
    std::cout << "Kmers size: " << kmers_.size() << '\n';
    std::vector<HashFunction> hashFunctions;
    hashFunctions.push_back(djb2);
    hashFunctions.push_back(sdbm);

    start = std::chrono::system_clock::now();
    bloom_filter_ = new BF((unsigned int) (10 *  kmers_.size()), hashFunctions); // 10 * number of kmers    
    for (auto i = kmers_.begin(); i != kmers_.end(); ++i)  
    {
	    bloom_filter_ -> addElement(*i);
    }
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout<<"Populating filter: " << elapsed_seconds.count() << "s" << std::endl;
}

std::string KBF1::name(){ return "kbf1"; }
