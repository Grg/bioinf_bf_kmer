# libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns



def plot_lines(_20, _30, tag="kbf_0", title="fpr", num_reads = [100000, 250000, 500000,1000000,2000000]):
    # plot
    plt.title(title+", "+tag)
    df=pd.DataFrame({'x': num_reads, 'y': _20})
    plt.plot( 'x', 'y', data=df, linestyle='-', marker='o', label="20")

    df=pd.DataFrame({'x': num_reads, 'y': _30})
    plt.plot( 'x', 'y', data=df, linestyle='-', marker='o', label="30")
    plt.legend(loc='upper left')

    plt.show()



_20_kbf_0_fpr = [0.0329, 0.033193, 0.0349, 0.03188, 0.033109]
_30_kbf_0_fpr = [0.0324, 0.0336212, 0.0341125, 0.0333202, 0.0326176]
plot_lines(_20_kbf_0_fpr, _30_kbf_0_fpr, "kbf_0", "fpr")


_20_kbf_0_kmers_size = [4698307, 7692537, 10081104, 12968897, 19012680]
_30_kbf_0_kmers_size = [4349056, 7497274, 10180260, 13483072, 20337324]
plot_lines(_20_kbf_0_kmers_size, _30_kbf_0_kmers_size, "kbf_0", "kmers_size")


_20_kbf_1_fpr = [0.0106,0.009795,0.011345,0.0101931,0.010667]
_30_kbf_1_fpr = [0.0096,0.009289,0.009712,0.009401, 0.0091274]
plot_lines(_20_kbf_1_fpr, _30_kbf_1_fpr, "kbf_1", "fpr")

_20_kbf_1_kmers_size = [4698307,7692537,10081104,12968897,19012680]
_30_kbf_1_kmers_size = [4349056,7497274,10180260,13483072,20337324]
plot_lines(_20_kbf_1_kmers_size, _30_kbf_1_kmers_size, "kbf_1", "kmers_size")

_20_kbf_2_fpr = [0.000912,0.00081376,0.001006, 0.001141, 0.000903159]
_30_kbf_2_fpr = [0.000631,0.000762394,0.0007435, 0.000704176, 0.000615894]
plot_lines(_20_kbf_2_fpr, _30_kbf_2_fpr, "kbf_2", "fpr")

_20_kbf_2_kmers_size = [4698307,7692537,10081104, 12968897,19012680]
_30_kbf_2_kmers_size = [4349056,7497274,10180260,13483072,20337324]
plot_lines(_20_kbf_2_kmers_size, _30_kbf_2_kmers_size, "kbf_2", "kmers_size")

_20_strict_fpr = [0.0254,0.0256197,0.0261,0.02834,0.027988]
_30_strict_fpr = [0.0247,0.0251266,0.02541,0.024945,0.025901]
plot_lines(_20_strict_fpr, _30_strict_fpr, "strict", "fpr")

_20_strict_kmers_size = [2390014,3996664,5357166,6975774,10137260]
_30_strict_kmers_size = [2210232,3875273,5374915,7214744,10799718]
plot_lines(_20_strict_kmers_size, _30_strict_kmers_size, "strict", "kmers_size")

relaxed_num_reads = [10000,15000,20000,25000,30000]
_20_relaxed_fpr = [0.0326395,0.0317859, 0.0331966, 0.0355041, 0.0328604]
_30_relaxed_fpr = [0.030106,0.0310078, 0.0302124, 0.0304252,  0.0304543]
plot_lines(_20_relaxed_fpr, _30_relaxed_fpr, "relaxed", "pfr", relaxed_num_reads)

_20_relaxed_kmers_size = [485325,711469, 925795, 1130192, 1323771]
_30_relaxed_kmers_size = [423629,623299, 814300, 998090, 1174036]
plot_lines(_30_relaxed_kmers_size, _30_relaxed_kmers_size, "relaxed", "kmers_size", relaxed_num_reads)

