Position yourself to the build dir
```
cd build

```

remove cache if exists
```
rm CMakeCache.txt
```

Run cmake
```
cmake ..
```

Run make
```
make
```

Run program
```
./bioinf_bf_kmer

```

Alternatively all together
```
rm -f CMakeCache.txt  && cmake .. && make && ./bioinf_bf_kmer <algorithm> <kmer_len> <num_of_queries> <fp>
```

